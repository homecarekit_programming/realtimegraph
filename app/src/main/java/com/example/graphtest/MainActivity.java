package com.example.graphtest;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.LinearLayout;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
//import com.github.mikephil.charting.formatter.IAxisValueFormatter;
//import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    LineChart lineChart;
    int i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lineChart = (LineChart) findViewById(R.id.linechart);

        lineChart.setDescription("");
        lineChart.setNoDataTextDescription("No data for the moment");

        lineChart.setHighlightPerDragEnabled(true);

        lineChart.setTouchEnabled(true);

        lineChart.setDragEnabled(true);
        lineChart.setScaleEnabled(true);
        lineChart.setDrawGridBackground(false);

        lineChart.setPinchZoom(true);

        lineChart.setBackgroundColor(Color.WHITE);

        // 이제 데이터 시작합니다

        LineData data = new LineData();
        //data.setValueTextColor(Color.WHITE);

        //데이터 추가합니다

        lineChart.setData(data);

        //legend 객체

       /* Legend l = lineChart.getLegend();

        // legend 커스텀

        l.setForm(Legend.LegendForm.LINE);
        l.setTextColor(Color.WHITE);*/

        XAxis xl = lineChart.getXAxis();
        //xl.setPosition(XAxis.XAxisPosition.BOTTOM);
        //xl.setTextColor(Color.WHITE);
        xl.setDrawGridLines(false); // Y축 모양 grid 보이기
        xl.setAvoidFirstLastClipping(true);
        xl.setEnabled(true);

        YAxis yl = lineChart.getAxisLeft();
        //yl.setTextColor(Color.WHITE);
        yl.setAxisMinValue(-100f);
        yl.setAxisMaxValue(100f);
        yl.setDrawGridLines(true);
        yl.setStartAtZero(false);

        YAxis yl2 = lineChart.getAxisRight();
        yl2.setEnabled(false);




       /* ArrayList<String> labels = new ArrayList<String>();
        labels.add("January");
        labels.add("February");
        labels.add("March");
        labels.add("April");
        labels.add("May");
        labels.add("June");

        ArrayList<Entry> entries = new ArrayList<>();
        entries.add(new Entry(-4f, 0));
        entries.add(new Entry(8f, 1));
        entries.add(new Entry(-6f, 2));
        entries.add(new Entry(2f, 3));
        entries.add(new Entry(18f, 4));
        entries.add(new Entry(-9f, 5));

        LineChart lineChart = (LineChart) findViewById(R.id.linechart);

        lineChart.setDescriptionColor(Color.RED);
        lineChart.setGridBackgroundColor(Color.BLACK);



        YAxis yAxis1 = lineChart.getAxisLeft(); //Y축 최대최소
        yAxis1.setAxisMinValue(-20f);
        yAxis1.setAxisMaxValue(20f);

        XAxis xAxis = lineChart.getXAxis(); // X축 아래표시
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);

        YAxis yAxis2 = lineChart.getAxisRight(); // 오른쪽 Y축 감추기
        yAxis2.setEnabled(false);

        LineDataSet lineDataSet = new LineDataSet(entries, "SpO2");

        lineDataSet.setColors(ColorTemplate.VORDIPLOM_COLORS);
        lineDataSet.setDrawCubic(true);
        lineDataSet.setDrawValues(false);

        LineData lineData = new LineData(labels, lineDataSet);
        lineChart.setData(lineData); // set the data and list of lables into chart*/


    }

    //entry를 추가할 메소드가 필요하다.

    private void addEntry(int i) {
        LineData data = lineChart.getData();

        if (data != null) {
            LineDataSet set = data.getDataSetByIndex(0);

            if (set == null) {
                set = createSet();
                data.addDataSet(set);
            }

            data.addXValue("");
            data.addEntry(new Entry((float) (Math.random() * 50), set.getEntryCount()), 0);

            // 차트 데이터가 변경된걸 알려줌
            lineChart.notifyDataSetChanged();

            //볼수있는 entry의 개수 제한
            lineChart.setVisibleXRangeMaximum(6);

            //마지막 entry 스크롤
            lineChart.moveViewToX(data.getXValCount() - 7);
        }
    }

    private LineDataSet createSet() {
        LineDataSet set = new LineDataSet(null, "SpO2");

        set.setDrawCubic(true); // 직선에서 곡선 그래프로 만들어줌
        set.setCubicIntensity(0.2f);
        set.setAxisDependency(YAxis.AxisDependency.LEFT);
        set.setCircleColor(ColorTemplate.getHoloBlue()); // entry 지점 표시 동그라미 색
        set.setLineWidth(2f); //곡선두께
        set.setCircleSize(4f); // entry 지점 표시 동그라미 크기
        set.setFillAlpha(65);
        set.setFillColor(ColorTemplate.getHoloBlue());
        set.setHighLightColor(Color.rgb(244, 117, 177));
        set.setValueTextSize(2f); // entry 지점 값 글자 크기


        return set;
    }

    @Override
    protected void onResume() {
        super.onResume();

        new Thread(new Runnable() {
            @Override
            public void run() {
                for (i = 0; i < 10000; i++) {
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            addEntry(i);
                        }
                    });

                    try {
                        Thread.sleep(600);
                    } catch (InterruptedException e) {
                        //manage error
                    }
                }
            }
        }).start();
    }
}